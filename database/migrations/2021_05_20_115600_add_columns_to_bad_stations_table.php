<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToBadStationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
 
        Schema::connection('mysql')->table('bad_stations', function (Blueprint $table) {
            $sql = "ALTER TABLE `bad_stations`  ADD `checked` TINYINT(1) NOT NULL DEFAULT '0'  AFTER `updated_at`";
    		DB::connection('mysql')->getPdo()->exec($sql);
        });
        
        Schema::connection('mysql')->table('bad_stations', function (Blueprint $table) {
            $sql = "ALTER TABLE `bad_stations`  ADD `differ` TINYINT(1) NOT NULL DEFAULT '0'  AFTER `checked`";
    		DB::connection('mysql')->getPdo()->exec($sql);
        });
        
         Schema::connection('mysql')->table('bad_stations', function (Blueprint $table) {
            $sql = "ALTER TABLE `bad_stations`  ADD `differed_arr` VARCHAR(100) NULL  AFTER `differ`";
    		DB::connection('mysql')->getPdo()->exec($sql);
        });
        
         Schema::connection('mysql')->table('bad_stations', function (Blueprint $table) {
            $sql = "ALTER TABLE `bad_stations`  ADD `max_year` INT(4) NULL  AFTER `differed_arr`";
    		DB::connection('mysql')->getPdo()->exec($sql);
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('bad_stations', function (Blueprint $table) {
            $table->dropColumn('checked');
            $table->dropColumn('differ');
            $table->dropColumn('differed_arr');
            $table->dropColumn('max_year');
        });
    }
}
