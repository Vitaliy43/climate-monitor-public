<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tableinfo/{number}', function($number){
    $table = 'tutiempo_buffer_'.$number;
    $sql = "SELECT * FROM $table WHERE mintemp > midtemp OR mintemp > maxtemp";
	$data = DB::connection('mysql2')->select(DB::raw($sql));

    $stations_with_errors = array();
    $buffer = array();
    foreach( $data as $item ){
        $buffer[] = $item->station;
    }
    $stations_with_errors = array_unique($buffer);
    
    if( count($stations_with_errors) > 0 ){
        foreach( $stations_with_errors as $station ){
        	$date = date('Y-m-d h:i:s');
            $arr = array($station,$number,$date,$date);
            DB::connection('mysql')->insert('INSERT IGNORE INTO bad_stations (station,table_id,created_at,updated_at) VALUES(?,?,?,?)',$arr);
        }
    }
    
    return view('tableinfo', ['data' => $data] );
});

Route::get('station/{id}', 'ClimateController@index');

Route::get('queue/{operation}', 'ClimateController@queue');