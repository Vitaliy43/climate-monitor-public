<?php

if( count($data) > 0 ){
	echo '<div>Всего записей: '.count($data).'</div>';
    echo '<table>';
    echo '<tr><th>id</th>station</th><th>date</th><th>temps</th></tr>';
    foreach( $data as $item ){
        echo '<tr>';
        echo '<td>'.$item->id.'</td>';
        echo '<td>'.$item->station.'</td>';
        echo '<td>'.$item->year.'-'.$item->month.'-'.$item->day.'</td>';
        echo '<td>'.$item->mintemp.' ; '.$item->midtemp.' ; '.$item->maxtemp.'</td>';
        echo '</tr>';
    }
    echo '</table>';
}
else{
    echo 'No results';
}

?>