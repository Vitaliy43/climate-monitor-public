<?php

namespace App\Classes;

use Illuminate\Support\Facades\DB;
use App\Models\HighestTemp;
use App\Models\LowestTemp;

class Climate {

	protected static $station;
	protected static $table;
	protected static $differ = array();
	public static $operation;
	public static $log_info = true;
	public static $methods = array('getHighestTemperature','getLowestTemperature');
	protected static $months_names = array(
		1 => 'jan',
		2 => 'feb',
		3 => 'mar',
		4 => 'apr',
		5 => 'may',
		6 => 'jun',
		7 => 'jul',
		8 => 'aug',
		9 => 'sep',
		10 => 'oct',
		11 => 'nov',
		12 => 'dec'
	);
	
	/**
	* Запускаем процедуру сверки данных из архива погоды с данными на сайте climatebase
	* @param integer $station
	* @param integer $table_id
	* return void
	*/
	public static function execute($station,$table_id){
		self::$station = $station;
		self::$table = 'tutiempo_buffer_'.$table_id;
		if( !self::$log_info ){
			ob_start();
		}
		if( self::$operation == 'differ' ){
			foreach( self::$methods as $method ){
				self::$method();
			}
			self::setResult();
			if( !self::$log_info ){
				ob_end_clean();
			}
		}
		
	}
	
	/**
	* Метод апдейта таблицы и занесения резульатов
	* 
	* @return void
	*/
	protected static function setResult(){
		$sql = "SELECT MAX(year) AS year FROM ".self::$table." WHERE station = ".self::$station;
		$buffer = DB::connection('mysql2')->select(DB::raw($sql));
		$max_year = $buffer[0]->year;
		$arr = array('checked' => 1,'max_year' => $max_year);
		if( count(self::$differ) > 0 ){
			$arr['differ'] = 1;
			$arr['differed_arr'] = json_encode(self::$differ);
		}
		else{
			$arr['differ'] = 0;
		}
		$sql = "UPDATE bad_stations ".self::$table." WHERE station = ".self::$station;
		
		$result = DB::connection('mysql')->table('bad_stations')->where('station',self::$station)->update($arr);
		if( $result && self::$log_info == true ){
			echo '<b>Record updated! </b>';
		}
	}

	/**
	* Метод сверки максимальных температур
	* return void
	*/
	protected static function getHighestTemperature(){
		$data_from_archive = array();
		$data_from_climatebase = array();
		$sql = "SELECT MAX(maxtemp) AS highest FROM ".self::$table." WHERE station = ".self::$station;
		$buffer = DB::connection('mysql2')->select(DB::raw($sql));
		$data_from_archive['year'] = $buffer[0]->highest;
		
		foreach( self::$months_names as $number=>$name ){
			$sql = "SELECT MAX(maxtemp) AS highest FROM ".self::$table." WHERE station = ".self::$station." AND month = $number";
			$buffer = DB::connection('mysql2')->select(DB::raw($sql));
			$data_from_archive[$name] = $buffer[0]->highest;
		}
		echo '<h3>Highest temperature </h3>';
		echo '<pre>';
		print_r($data_from_archive);
		echo '</pre>';
		echo '<br>';
		

		$data_from_climatebase['year'] = HighestTemp::where('station',self::$station)->max('year');
		if( !$data_from_climatebase['year'] ) return;
		
		foreach( self::$months_names as $number=>$name ){
			$data_from_climatebase[$name] = HighestTemp::where('station',self::$station)->max($name);
		}
		echo '<pre>';
		print_r($data_from_climatebase);
		echo '</pre>';
		echo '<br>';
		
		
		$buffer = array_diff($data_from_archive,$data_from_climatebase);

		if( count($buffer) > 0 ){
			echo '<b>Differs:</b>';
			echo '<pre>';
			print_r($buffer);
			echo '</pre>';
			
			self::$differ['highest_temperature'] = json_encode($buffer);
		}
		echo '<br>';
	}
	
	/**
	* Метод сверки минимальных температур
	* return void
	*/
	protected static function getLowestTemperature(){
		$data_from_archive = array();
		$data_from_climatebase = array();
		$sql = "SELECT MIN(mintemp) AS lowest FROM ".self::$table." WHERE station = ".self::$station;
		$buffer = DB::connection('mysql2')->select(DB::raw($sql));
		$data_from_archive['year'] = $buffer[0]->lowest;	
		
		foreach( self::$months_names as $number=>$name ){
			$sql = "SELECT MIN(mintemp) AS lowest FROM ".self::$table." WHERE station = ".self::$station." AND month = $number";
			$buffer = DB::connection('mysql2')->select(DB::raw($sql));
			$data_from_archive[$name] = $buffer[0]->lowest;
		}
		
		echo '<h3>Lowest temperature </h3>';
		echo '<pre>';
		print_r($data_from_archive);
		echo '</pre>';
		echo '<br>';
		

		$data_from_climatebase['year'] = LowestTemp::where('station',self::$station)->min('year');
		if( !$data_from_climatebase['year'] ) return;

		foreach( self::$months_names as $number=>$name ){
			$data_from_climatebase[$name] = LowestTemp::where('station',self::$station)->min($name);
		}
		echo '<pre>';
		print_r($data_from_climatebase);
		echo '</pre>';
		echo '<br>';
		
		$buffer = array_diff($data_from_archive,$data_from_climatebase);

		if( count($buffer) > 0 ){
			echo '<b>Differs:</b>';
			echo '<pre>';
			print_r($buffer);
			echo '</pre>';
			self::$differ['lowest_temperature'] = json_encode($buffer);

		}
		

	}
	
}

?>