<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HighestTemp extends Model
{
    use HasFactory;
    
    protected $table = 'highest_temperature';
    protected $connection = 'mysql';
}
