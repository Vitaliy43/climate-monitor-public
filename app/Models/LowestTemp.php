<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LowestTemp extends Model
{
    use HasFactory;
    
    protected $table = 'lowest_temperature';
    protected $connection = 'mysql';
}
