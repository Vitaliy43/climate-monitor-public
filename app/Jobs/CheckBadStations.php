<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Classes\Climate;

class CheckBadStations implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
     protected $station;
     protected $table_id;
     protected $oprtation;
     
     
    public function __construct($operation,$station,$table_id)
    {
    	$this->oprtation = $operation;
        $this->station = $station;
        $this->table_id = $table_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Climate::$operation = $this->oprtation;
        Climate::$log_info = false;
  		Climate::execute($this->station,$this->table_id);
    }
}
