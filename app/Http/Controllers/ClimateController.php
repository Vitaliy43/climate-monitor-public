<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Station;
use App\Models\Weather;
use App\Models\BadStations;
use App\Classes\Climate;
use App\Jobs\CheckBadStations;

class ClimateController extends Controller
{
	
	public function index($station)
  	{
  		$data = Station::where('station',$station)->first();
  		Climate::$operation = 'differ';
  		Climate::execute($station,$data->table_id);
		  
  		echo 'table_id '.$data->table_id.'<br>';
  		echo 'station '.$station.'<br>';

  	}
  	
  	public function queue($operation){
		$limit = 100;	
		$data = BadStations::where('checked',0)->limit($limit)->get();
		foreach( $data as $item ){
			CheckBadStations::dispatch($operation,$item->station,$item->table_id);
		}
		
	}
}
